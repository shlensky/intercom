var path = require('path');
var webpack = require('webpack');
var CleanPlugin = require('clean-webpack-plugin');
var HtmlPlugin = require('html-webpack-plugin');
var CopyPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {app: './src/app.js', widget: './src/widget/widget.js'},
    output: {
        path: './dist',
        filename: 'scripts-[name]-[chunkhash].js'
    },
    module: {
        preLoaders: [
            {test: /\.js$/, loader: 'eslint', exclude: /(node_modules|flatkit)/}
        ],
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|flatkit)/,
                loaders: ['ng-annotate', 'babel?presets=es2015']
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style', 'css', 'autoprefixer?browsers=last 2 version')
            },
            {
                test: /\.scss/,
                loader: ExtractTextPlugin.extract('style', 'css!autoprefixer?browsers=last 2 version!sass')
            },
            {
                test: /\.(eot|ttf|woff|woff2|svg|svgz)$/,
                loader: 'file?name=fonts/[name].[ext]'
            },
            {
                test: /\.html$/,
                exclude: /node_modules/,
                loader: 'ngtemplate?relativeTo=' + (path.resolve(__dirname, './src')) + '/!html?interpolate&collapseWhitespace=false'
            },
            {
                test: /\.handlebars$/,
                loader: 'handlebars-loader'
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.(png|jpg|jpeg)/,
                loader: 'file-loader?name=[name]-[hash].[ext]'
            }
        ]
    },
    plugins: [
        new CleanPlugin(['dist']),
        new HtmlPlugin({
            template: 'src/index.html',
            inject: 'body'
        }),
        new CopyPlugin([
            {from: 'src/favicon.ico'}
        ]),
        new ExtractTextPlugin('styles-[hash].css'),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            "window.jQuery": 'jquery',
            "_": 'lodash'
        }),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/)
    ],
    //devtool: 'inline-source-map',
    devServer: {
        port: 9902
    }
};
