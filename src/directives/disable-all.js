module.exports = angular.module('intercom.directives.disableAll', [])
    .directive('disableAll', function() {
        return {
            restrict: 'A',
            scope: false,
            compile: function(elem, attrs) {

                var attr = attrs.disableAll;
                var elems = elem.find('[ng-model],[ng-click],[ng-change],[ng-disabled],button,input');

                elems = elems.addBack(elem[0]);

                elems.each(function() {
                    if (this.hasAttribute('no-disable-all')) return;

                    if (this.getAttribute('ng-disabled')) {
                        var value = this.getAttribute('ng-disabled') + ' || ' + attr;
                        this.setAttribute('ng-disabled', value);
                    } else {
                        this.setAttribute('ng-disabled', attr);
                    }
                });
            }
        }
    });
