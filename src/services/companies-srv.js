var faker = require('faker');

module.exports = angular.module('intercom.services.companiesSrv', [
    require('./users-srv.js').name
])
    .service('companiesSrv', (usersSrv)=> {
        let companies = [];

        let idSeed = 1;
        _.times(8, ()=> {
            // Tags & Segments
            let tags = _(7).times(()=> faker.commerce.productAdjective()).uniq().value();
            let segments = _(5).times(()=> faker.commerce.department()).uniq().value();

            // Notes
            let notes = _(3).times((i) => {
                return {
                    user: usersSrv.getOne(i+3),
                    createdAt: faker.date.recent(i),
                    text: faker.company.catchPhrase()
                }
            }).sortBy('createdAt').reverse().value();

            companies.push({
                id: idSeed++,
                name: faker.company.companyName(),
                createdAt: faker.date.past(),
                lastSeenAt: faker.date.recent(),
                lastContactedAt: faker.date.recent(),
                sessionCount: faker.random.number(1500),
                segments, tags, notes
            });
        });

        class companiesSrv {
            getAll() {
                return companies;
            }

            getByIndex(index) {
                return companies[index];
            }

            getById(id) {
                return _.find(companies, {id});
            }

            count() {
                return companies.length;
            }
        }

        return new companiesSrv();
    });
