var faker = require('faker');

module.exports = angular.module('intercom.services.customersSrv', [
    require('./users-srv.js').name,
    require('./companies-srv.js').name
])
    .service('customersSrv', (usersSrv, companiesSrv)=> {
        let customers = [];

        let idSeed = 382;
        let avatarSeed = faker.random.number();
        _.times(186, (i)=> {
            let card = faker.helpers.createCard();

            let pairs = [[37.3523199, 55.7494733], [5.9691811, 51.0782589]];
            let coords = _.sample(pairs).join(',');

            let mapUrl = `https://api.mapbox.com/v4/mapbox.satellite/pin-s(${coords})/${coords},2/1280x186.png?access_token=pk.eyJ1Ijoic2hsZW5za3kiLCJhIjoiY2lqZXF5cjNwMDAwMHcza3Jld3RkZDV0eCJ9.D_Lt97FMw730T5DeFpgDAA`;

            // Events
            let eventTypes = [
                {name: 'Tagged', icon: 'tag', color: 'info'},
                {name: 'Untagged', icon: 'tag', color: 'accent'},
                {name: 'Entered', icon: 'pie-chart', color: 'warn'},
                {name: 'Upgraded-plan', icon: 'calendar', color: 'warning'}
            ];
            let events = [];

            _.times(10, (i)=> {
                let type = eventTypes[i % 4];
                events.push({
                    type,
                    name: (type.name == 'Tagged' || type.name == 'Untagged') ?
                        faker.commerce.productAdjective() :
                        (type.name == 'Entered' ? faker.commerce.department() : faker.commerce.productMaterial()),
                    createdAt: faker.date.recent(i)
                });
                events = _(events).sortBy('createdAt').reverse().value();
            });

            // Tags & Segments
            let tags = _(7).times(()=> faker.commerce.productAdjective()).uniq().value();
            let segments = _(5).times(()=> faker.commerce.department()).uniq().value();

            // Notes
            let notes = _(3).times((i) => {
                return {
                    user: usersSrv.getOne(i+3),
                    createdAt: faker.date.recent(i),
                    text: faker.company.catchPhrase()
                }
            }).sortBy('createdAt').reverse().value();

            let company = _.sample([null, companiesSrv.getByIndex(i % companiesSrv.count())]);
            customers.push({
                id: idSeed++,
                name: card.name,
                email: card.email,
                createdAt: faker.date.past(),
                lastSeenAt: faker.date.recent(),
                lastContactedAt: faker.date.recent(),
                sessionCount: faker.random.number(150),
                avatar: require(`../../flatkit/assets/images/b${++avatarSeed % 9}.jpg`),
                company,
                countryName: card.address.country,
                cityName: card.address.city,
                localTime: _.now(),
                browser: _.sample(['chrome', 'internet explorer', 'edge', 'firefox', 'opera']),
                browserVersion: '47.0.2526.106',
                mapUrl: `url("${mapUrl}")`,
                events, segments, tags, notes
            });
        });

        _.each(companiesSrv.getAll(), (company)=> {
            company.customers = _.where(customers, {company: company});
            company.customerCount = company.customers.length;
        });

        class CustomersSrv {
            getAll() {
                return customers;
            }

            getOne(index) {
                return customers[index];
            }

            getById(id) {
                return _.find(customers, {id});
            }
        }

        return new CustomersSrv();
    });
