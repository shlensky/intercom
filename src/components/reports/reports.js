/**
 * Reports page.
 */
//var faker = require('faker');
var templateUrl = require('./reports.html');
require('./types/users.html');
require('./types/segments.html');
require('./types/tags.html');

require('../../../flatkit/libs/js/echarts/build/dist/echarts-all.js');
window.theme = require('../../../flatkit/libs/js/echarts/build/dist/theme.js'); // eslint-disable-line
require('../../../flatkit/libs/js/echarts/build/dist/jquery.echarts.js');

module.exports = angular.module('intercom.components.reports', [])

    .directive('reports', function($stateParams, $filter, moment) {
        class Controller {
            constructor() {
                this.types = [
                    {alias: 'users', name: 'Users', icon: 'users'},
                    {alias: 'segments', name: 'Segments', icon: 'pie-chart'},
                    {alias: 'tags', name: 'Tags', icon: 'tag'}
                ];
                this.type = _.find(this.types, {alias: $stateParams.type}) || _.first(this.types);


                this.groupings = ['Week', 'Month', 'Quarter', 'Year'];
                this.grouping = _.first(this.groupings);

                this.setupTotal();
                this.setupByPeriod();
                this.setupByWeekday();
            }

            setupTotal() {
                let items = require('./data/users.json');
                this.chartTotal = {
                    tooltip: {},
                    xAxis: [
                        {
                            type: 'category',
                            boundaryGap: false,
                            data: _.map(items, (item)=> $filter('date')(item.x))
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            scale: true
                        }
                    ],
                    series: [
                        {
                            name: 'Users',
                            type: 'line',
                            smooth: true,
                            clickable: false,
                            itemStyle: {normal: {areaStyle: {type: 'default'}}},
                            data: _.map(items, (item)=> item.y)
                        }
                    ]
                };
            }

            setupByPeriod() {
                let {groups, data} = this.calculate(this.grouping);

                this.chartByPeriod = {
                    tooltip: {},
                    xAxis: [{type: 'category', data: groups.keys()}],
                    yAxis: [{type: 'value'}],
                    series: [{name: 'Users', type: 'bar', clickable: false, data: data.value()}]
                };
            }

            setupByWeekday() {
                let {groups, data} = this.calculate('Weekday', 'percent');

                this.chartByWeekday = {
                    tooltip: {
                        formatter: '{c}%'
                    },
                    xAxis: [{type: 'category', data: groups.keys()}],
                    yAxis: [{type: 'value'}],
                    series: [{name: 'Users', type: 'bar', clickable: false, data: data.value()}]
                };
            }

            calculate(grouping, valueType = 'value') {
                let items = require('./data/users.json');

                let groups = _(items).groupBy((item)=> {

                    switch (grouping) {
                        case 'Weekday':
                            return moment(item.x).format('dddd');

                        case 'Week':
                            return [
                                moment(item.x).startOf('week').format('MMM Do'),
                                moment(item.x).endOf('week').format('MMM Do')].join(' - ');

                        case 'Month':
                            return moment(item.x).format('MMMM YYYY');

                        case 'Quarter':
                            return moment(item.x).format('[Q]Q YYYY');

                        case 'Year':
                            return (new Date(item.x)).getFullYear();
                    }
                });

                let data = groups.map((group)=> {

                    // there is a bug, first day of each group is not calculated
                    let count = 0;
                    _(group).map('y').reduce((sum, y)=> {
                        count += (y - sum);
                        return y;
                    });

                    return count;
                });

                // Convert absolute values to percents
                if (valueType == 'percent') {
                    let total = _(data).sum();
                    data = data.map((value)=> (value / (total / 100)).toFixed(2));
                }

                return {groups, data};
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
