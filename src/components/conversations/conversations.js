var templateUrl = require('./conversations.html');
var faker = require('faker');

module.exports = angular.module('intercom.components.conversations', [
    require('../../services/customers-srv.js').name,
    require('../refer-element/refer-element.js').name
])

    .directive('conversations', function($timeout, customersSrv) {
        class Controller {
            constructor() {
                this.user = {
                    name: 'John Smith',
                    avatar: require('../../../flatkit/assets/images/a0.jpg')
                };

                this.inboxes = [
                    {
                        name: this.user.name,
                        avatar: this.user.avatar,
                        count: 20
                    },
                    {name: 'Unassigned', icon: 'user', count: 3},
                    {name: 'Team', icon: 'users', count: 140, multiselect: true},
                    {name: 'All', icon: 'user', count: 163}
                ];
                this.selectedInbox = _.first(this.inboxes);


                this.conversations = _.times(9, (i)=> {
                    let customer = customersSrv.getOne(i);
                    let conversation = {
                        customer: customer,
                        messages: []
                    };

                    _.times(20, (i)=> {
                        let message = {
                            text: faker.hacker.phrase(),
                            createdAt: faker.date.recent()
                        };

                        if (i % 2) {
                            message.user = this.user;
                        } else {
                            message.customer = customer;
                        }

                        conversation.messages.push(message);
                    });
                    conversation.messages = _(conversation.messages).sortBy('createdAt').value();
                    conversation.lastMessage = _(conversation.messages).filter('customer').last();

                    return conversation;
                });
                this.conversations = _(this.conversations).sortBy('lastMessage.createdAt').reverse().value();
                this.selectConversation(_.first(this.conversations));
            }

            selectConversation(conversation) {
                this.selectedConversation = conversation;
                this.scrollToBottom();
            }

            reply(text) {
                this.selectedConversation.messages.push({
                    user: this.user,
                    createdAt: _.now(),
                    text
                });

                this.scrollToBottom();
                this.replyText = '';
            }

            scrollToBottom() {
                $timeout(()=> {
                    if (this.messagesContainer) {
                        this.messagesContainer.scrollTop = this.messagesContainer.scrollHeight;
                    }
                });
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
