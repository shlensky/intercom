var templateUrl = require('./users-segments.html');

module.exports = angular.module('intercom.components.usersSegments', [
    'ui.router',
    require('../../services/segments-srv.js').name
])

    .directive('usersSegments', function($state, segmentsSrv) {
        class Controller {
            constructor() {
                this.segments = this.companyMode ? segmentsSrv.getCompanySegments() : segmentsSrv.getSegments();
                this.selectedSegment = _.find(this.segments, {id: $state.params.segmentId});
                this.listState = this.companyMode ? 'companies' : 'customers';
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {
                companyMode: '='
            },
            bindToController: true
        };
    });
