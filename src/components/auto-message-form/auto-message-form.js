/**
 * Auto message wizzard.
 */

var templateUrl = require('./auto-message-form.html');
_.times(7, (i)=> {
    require(`./steps/step-${i + 1}.html`);
    require(`./steps/step-${i + 1}-info.html`);
});

//var faker = require('faker');

module.exports = angular.module('intercom.components.autoMessageForm', [
    'textAngular',
    require('../../services/users-srv.js').name,
    require('../../services/customers-srv.js').name,
    require('../../services/email-templates-srv.js').name
])

    .directive('autoMessageForm', function(usersSrv, customersSrv, emailTemplatesSrv) {
        class Controller {
            constructor() {
                this.name = '';
                this.messageBody = '';

                this.users = usersSrv.getAll();
                this.sender = _.first(this.users);
                this.receiver = _.first(this.users);
                this.customers = customersSrv.getAll();
                this.templates = emailTemplatesSrv.getAll();
                this.template = _.first(this.templates);

                this.nameStep = {
                    title: 'Name',
                    description: 'Name your message',
                    isCompleted: ()=> !!this.name
                };

                this.audienceStep = {
                    title: 'Audience',
                    description: 'Choose your audience',
                    isCompleted: ()=> _.any(this.audienceStep.selectedAttributes, (attribute)=> attribute.selected && attribute.predicates.length),
                    selectedAttributes: []
                };

                this.channelStep = {
                    title: 'Channel',
                    description: 'Select your channel',
                    isCompleted: ()=> this.isChannelSelectedByUser,
                    onExpand: ()=> this.isChannelSelectedByUser = true
                };

                this.messageStep = {
                    title: 'Message',
                    description: 'Write your message',
                    isCompleted: ()=> !!this.messageBody.length
                };

                this.isStopDateSelectedByUser = false;
                this.stopDate = _.now();
                this.stopDateStep = {
                    isOptional: true,
                    title: 'Stop date',
                    description: 'Set a stop date',
                    isCompleted: ()=> this.isStopDateSelectedByUser,
                    onExpand: ()=> this.isStopDateSelectedByUser = true
                };

                this.isGoalSelectedByUser = false;
                this.goalStep = {
                    isOptional: true,
                    title: 'Goal',
                    description: 'Set a goal',
                    isCompleted: ()=> this.isGoalSelectedByUser,
                    onExpand: ()=> this.isGoalSelectedByUser = true,
                    goal: null
                };

                this.reviewStep = {
                    title: 'Review',
                    description: 'Review and set it live',
                    isCompleted: ()=> false
                };

                this.steps = [
                    this.nameStep,
                    this.audienceStep,
                    this.channelStep,
                    this.messageStep,
                    this.stopDateStep,
                    this.goalStep,
                    this.reviewStep
                ];


                this.inappTypes = [
                    {name: 'Chat', icon: 'comment-o'},
                    {name: 'Small Announcement', icon: 'bell-o'},
                    {name: 'Big Announcement', icon: 'bullhorn'}
                ];
                this.replyTypes = [
                    {name: 'Text', icon: 'commenting-o'},
                    {name: 'Thumbs', icon: 'thumbs-o-up'},
                    {name: 'Emotions', icon: 'smile-o'}
                ];
                this.channels = [
                    {
                        alias: 'inapp',
                        name: 'In-App Message',
                        title: 'In-app message',
                        icon: 'comment-o',
                        advantages: [
                            'Delivered inside your product',
                            'Up to 10x more engaging than email',
                            'Perfect for reaching active users'
                        ],
                        targetPage: false,
                        type: _.first(this.inappTypes),
                        replyType: _.first(this.replyTypes)
                    },
                    {
                        alias: 'email',
                        name: 'Email',
                        title: 'Email message',
                        icon: 'envelope-o',
                        advantages: [
                            'Delivered to a user\'s email inbox',
                            'Use your own custom email templates',
                            'Ideal for re-engaging inactive users'
                        ],
                        includeUnsubscribe: true
                    }
                ];
                this.selectedChannel = _.first(this.channels);
                this.isChannelSelectedByUser = false;


                this.previewTypes = [
                    {name: 'Desktop', icon: 'desktop'},
                    {name: 'Mobile', icon: 'mobile'}
                ];
                this.previewType = _.first(this.previewTypes);

                this.previewStates = [
                    {name: 'Opened', icon: 'expand'},
                    {name: 'Initial', icon: 'compress'}
                ];
                this.previewState = _.first(this.previewStates);

                this.previewDataTypes = [
                    {name: 'Placeholders', icon: 'ellipsis-h'},
                    {name: 'Sample user', icon: 'user'}
                ];
                this.previewDataType = _.first(this.previewDataTypes);
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
