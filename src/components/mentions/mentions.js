/**
 * Mentions page.
 */
var faker = require('faker');
var templateUrl = require('./mentions.html');

module.exports = angular.module('intercom.components.mentions', [
    require('../../services/customers-srv.js').name,
    require('../../services/users-srv.js').name
])

    .directive('mentions', function(customersSrv, usersSrv) {
        class Controller {
            constructor() {
                this.currentUser = usersSrv.getOne(0);
                let events = [];

                _.times(25, (i)=> {
                    let user = usersSrv.getOne((i + 1) % 9);
                    let customer = customersSrv.getOne(i);

                    events.push({
                        createdAt: faker.date.recent(i / 2),
                        customer,
                        user,
                        text: `${faker.hacker.phrase()}`
                    });
                });

                this.events = _(events).sortBy('createdAt').reverse().value();
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
