module.exports = angular.module('intercom.components.stCell', ['smart-table'])
    .directive('stCell', function() {
        return {
            restrict: 'A',
            require: '^stTable',
            scope: {
              options: '=stCell'
            },
            link: function(scope, element, attrs, table) {

                let $element = element[0];

                // Table cell
                if ($element.nodeName === 'TD') {
                    let header = table.headers[attrs.stCell];
                    header.cells.push({
                        element: $element
                    });

                    scope.$on('$destroy', ()=> {
                        _.remove(header.cells, {element: $element});
                    });

                    $element.style.display = header.visible ? 'table-cell' : 'none';

                    return;
                }


                // Table header

                // Initialize headers array on the table controller
                if (!table.headers) table.headers = [];

                let header = {
                    name: $element.innerText,
                    element: $element,
                    visible: true,
                    allowHide: true,
                    cells: []
                };
                if (scope.options) _.assign(header, scope.options);
                table.headers.push(header);

                // Remove header from table after scope destroyed
                //scope.$on('$destroy', function () {
                //    remove header
                //});

                // In case if checkedRows will be modified somewhere else
                scope.$watch(()=> header.visible, ()=> {
                    header.element.style.display = header.visible ? 'table-cell' : 'none';
                    _.each(header.cells, (cell)=> cell.element.style.display = header.visible ? 'table-cell' : 'none')
                });
            }
        }
    });
