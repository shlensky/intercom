/**
 * List of automations.
 */

var templateUrl = require('./automations.html');
var faker = require('faker');

module.exports = angular.module('intercom.components.automations', [
    require('../../services/users-srv.js').name
])

    .directive('automations', function(usersSrv) {
        class Controller {
            constructor() {
                this.audiences = ['users', 'visitors'];
                this.audience = 'users';

                this.folders = [
                    {name: 'All', count: 12, icon: 'list'},
                    {name: 'All live', count: 0, icon: 'bolt'},
                    {name: 'All drafts', count: 12, icon: 'pencil'},
                    {name: 'All paused', count: 0, icon: 'pause-circle'},
                    {name: 'All scheduled', count: 0, icon: 'calendar'},
                    {name: 'My messages', count: 4, icon: 'user'}
                ];
                this.folder = _.first(this.folders);

                this.records = [];
                _.times(186, ()=> {
                    let state = _.sample(['draft', 'paused', 'sending', '']);
                    this.records.push({
                        title: `${faker.commerce.productName()}`,
                        from: _.sample(usersSrv.getAll()),
                        sends: state == 'draft' ? 0 : faker.random.number({min: 500, max: 999}),
                        opens: state == 'draft' ? 0 : faker.random.number({min: 100, max: 500}),
                        clicks: state == 'draft' ? 0 : faker.random.number({min: 0, max: 100}),
                        lastSentAt: state == 'sending' ? faker.date.recent() : null,
                        createdAt: faker.date.past(),
                        type: _.sample(['small_announcement', 'email']),
                        state
                    });
                });
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
