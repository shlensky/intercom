var templateUrl = require('./users-filter.html');
var modalTemplateUrl = require('./create-segment-modal.html');

require('./predicates/date.html');
require('./predicates/string.html');

module.exports = angular.module('intercom.components.usersFilter', [
    'ui.bootstrap.modal'
])

    .directive('usersFilter', function($state, $uibModal, segmentsSrv) {

        class ModalController {
            constructor($uibModalInstance, segment, segments, attributes) {
                this.$uibModalInstance = $uibModalInstance;
                this.segment = segment;
                this.segments = segments;
                this.attributes = attributes;

                this.mode = 'create';
                this.name = '';

                this.predicates = [];

                // Collect predicates from filter
                _.each(this.attributes, (attribute)=> {
                    _.each(attribute.predicates, (predicate)=> {
                        if (!predicate.comparison) return;

                        this.predicates.push({
                            type: attribute.type,
                            attribute: attribute.alias,
                            comparison: predicate.comparison.alias,
                            value: predicate.value
                        });
                    });
                });
            }

            update() {
                this.segment.predicates = this.predicates;
                this.$uibModalInstance.close();
            }

            create() {
                let segment = {
                    id: _.max(_.pluck(this.segments, 'id')) + 1,
                    name: this.name,
                    count: 0,
                    isPredefined: false,
                    isEditable: true,
                    predicates: this.predicates,
                    icon: 'pie-chart'
                };

                this.segments.push(segment);
                this.$uibModalInstance.close(segment);
            }
        }

        class Controller {
            constructor() {
                this.attributes = this.companyMode ? this.companyAttributes() : this.userAttributes();
                this.segments = this.companyMode ? segmentsSrv.getCompanySegments() : segmentsSrv.getSegments();

                if (this.exposeAttributes) this.exposeAttributes = this.attributes;

                this.comparisons = {
                    string: [
                        {name: 'is',                alias: 'is',            showInput: true},
                        {name: 'is not',            alias: 'is_not',        showInput: true},
                        {name: 'starts with',       alias: 'starts',        showInput: true},
                        {name: 'ends with',         alias: 'ends',          showInput: true},
                        {name: 'contains',          alias: 'contains',      showInput: true},
                        {name: 'does not contain',  alias: 'not_contains',  showInput: true},
                        {name: 'is unknown',        alias: 'is_unknown',    showInput: false},
                        {name: 'has any value',     alias: 'any',           showInput: false}
                    ],

                    date: [
                        {name: 'more than',     alias: 'more_than',     group: 'relative', showInput: true},
                        {name: 'exactly',       alias: 'exactly',       group: 'relative', showInput: true},
                        {name: 'less than',     alias: 'less_than',     group: 'relative', showInput: true},
                        {name: 'after',         alias: 'after',         group: 'absolute', showInput: true},
                        {name: 'on',            alias: 'on',            group: 'absolute', showInput: true},
                        {name: 'before',        alias: 'before',        group: 'absolute', showInput: true},
                        {name: 'is unknown',    alias: 'is_unknown',    group: 'absolute', showInput: false},
                        {name: 'has any value', alias: 'any',           group: 'absolute', showInput: false}
                    ]
                };

                // Restore predicates from segment
                if ($state.params.segmentId) {
                    this.segment = _.find(this.segments, {id: $state.params.segmentId});
                    _.each(this.segment.predicates, (predicate)=> {
                        let attribute = _.find(this.attributes, {alias: predicate.attribute});
                        if (!attribute) return;

                        attribute.predicates.push({
                            value: predicate.value,
                            comparison: _.find(this.comparisons[attribute.type], {alias: predicate.comparison})
                        });

                        attribute.selected = true;
                    });
                }

                // for dirty checking
                this.prestineCopy = JSON.stringify(this.attributes);
            }

            addPredicate(attribute, predicate) {
                attribute.predicates.push(predicate);
            }

            removePredicate(attribute, predicate) {
                _.pull(attribute.predicates, predicate);
                if (!attribute.predicates.length) attribute.selected = false;
            }

            canAddNewPredicate(attribute) {
                let lastPredicate = _.last(attribute.predicates);
                if (!lastPredicate.comparison) return false;
                if (!lastPredicate.comparison.showInput) return true;

                return !!lastPredicate.value;
            }

            createSegment() {
                var modalInstance = $uibModal.open({
                    templateUrl: modalTemplateUrl,
                    size: 'md',
                    controller: ['$uibModalInstance', 'segment', 'segments', 'attributes', ModalController],
                    controllerAs: 'ctrl',
                    resolve: {
                        segment: _.constant(this.segment),
                        segments: _.constant(this.segments),
                        attributes: _.constant(this.attributes)
                    }
                });

                modalInstance.result.then((createdSegment)=> {
                    if (createdSegment) {
                        $state.go('.', {segmentId: createdSegment.id});
                    }
                });
            }

            isPristine() {
                return this.prestineCopy == JSON.stringify(this.attributes);
            }

            userAttributes() {
                return [
                    {
                        name: 'Name',
                        alias: 'name',
                        icon: 'users',
                        type: 'string',
                        predicates: [],
                        selected: false
                    },
                    {
                        name: 'Signed up',
                        alias: 'signed_up',
                        icon: 'calendar-check-o',
                        type: 'date',
                        predicates: [],
                        selected: false
                    },
                    {
                        name: 'Email',
                        alias: 'email',
                        icon: 'at',
                        type: 'string',
                        predicates: [],
                        selected: false
                    },
                    {
                        name: 'Last seen',
                        alias: 'last_request_at',
                        icon: 'calendar-check-o',
                        type: 'date',
                        predicates: [],
                        selected: false
                    }
                ];
            }

            companyAttributes() {
                return [
                    {
                        name: 'Name',
                        alias: 'name',
                        icon: 'building',
                        type: 'string',
                        predicates: [],
                        selected: false
                    },
                    {
                        name: 'Created at',
                        alias: 'created_at',
                        icon: 'calendar-check-o',
                        type: 'date',
                        predicates: [],
                        selected: false
                    },
                    {
                        name: 'Last seen',
                        alias: 'last_request_at',
                        icon: 'calendar-check-o',
                        type: 'date',
                        predicates: [],
                        selected: false
                    },
                    {
                        name: 'Users',
                        alias: 'users',
                        icon: 'users',
                        type: 'string',
                        predicates: [],
                        selected: false
                    }
                ];
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {
                companyMode: '=',
                ruleMode: '=',
                exposeAttributes: '='
            },
            bindToController: true
        };
    });
