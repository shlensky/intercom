/**
 * Top menu.
 */

var templateUrl = require('./topmenu.html');

module.exports = angular.module('intercom.components.topmenu', [
    require('../../services/customers-srv.js').name,
    require('../../services/companies-srv.js').name
])

    .directive('topmenu', function($state, customersSrv, companiesSrv) {
        class Controller {
            constructor() {
                this.customers = customersSrv.getAll();
                this.companies = companiesSrv.getAll();
            }

            isAnyStateActive(states) {
                return _.any(states, (state)=> $state.includes(state))
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
