/**
 * List of companies.
 */

var templateUrl = require('./companies.html');

module.exports = angular.module('intercom.components.companies', [
    require('../../services/companies-srv.js').name,
    require('../../services/customers-srv.js').name
])

    .directive('companies', function(companiesSrv) {
        class Controller {
            constructor() {
                this.records = companiesSrv.getAll();
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
