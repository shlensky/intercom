/**
 * Activity page.
 */
var faker = require('faker');
var templateUrl = require('./activity.html');

module.exports = angular.module('intercom.components.activity', [
    require('../../services/customers-srv.js').name,
    require('../../services/users-srv.js').name
])

    .directive('activity', function(customersSrv, usersSrv) {
        class Controller {
            constructor() {
                // Events
                let eventTypes = [
                    {name: 'added the tag', preposition: 'to'},
                    {name: 'removed the tag', preposition: 'from'}
                ];
                let events = [];

                _.times(100, (i)=> {
                    let type = eventTypes[i % 2];
                    let user = usersSrv.getOne((i + 1) % 9);
                    let customer = customersSrv.getOne(i);

                    events.push({
                        type,
                        name: faker.commerce.productAdjective(),
                        createdAt: faker.date.recent(i / 2),
                        customer,
                        user
                    });
                });

                this.events = _(events).sortBy('createdAt').reverse().value();
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
