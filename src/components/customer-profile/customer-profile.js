/**
 * Customer profile page.
 */

var templateUrl = require('./customer-profile.html');
require('./customer-profile.scss');

module.exports = angular.module('intercom.components.customerProfile', [
    require('../../services/customers-srv.js').name,
    require('../../services/users-srv.js').name,
    require('../../services/conversations-srv.js').name,
    require('../users-segments/users-segments').name
])

    .directive('customerProfile', function($stateParams, customersSrv, usersSrv, conversationsSrv) {
        class Controller {
            constructor() {
                this.noteText = '';
                this.customer = customersSrv.getById($stateParams.id);
                this.currentUser = usersSrv.getOne(0);
                this.conversations = conversationsSrv.getAll();
            }

            addNote(text) {
                if (!text) return;

                this.customer.notes.unshift({
                    user: usersSrv.getOne(0),
                    text,
                    createdAt: _.now()
                });

                this.noteText = '';
            }
        }

        return {
            restrict: 'E',
            templateUrl,
            controller: Controller,
            controllerAs: 'ctrl',
            scope: {},
            bindToController: true
        };
    });
